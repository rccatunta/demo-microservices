package res.rcatunta.svcdemo1;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@EnableDiscoveryClient
@RestController
public class SvcDemo1Application {

	public static void main(String[] args) {
		SpringApplication.run(SvcDemo1Application.class, args);
	}

	@GetMapping("/")
	public String home() {
		return "Hello World!";
	}
}
